# LiveCellCV



## What is LiveCellCV?

**LiveCellCV** will be a sample preparation robot that can be attached to microscopes.

By integrating **OpenCV** (**Open** Source **C**omputer **V**ision Library) and **ROS** (**R**obot **O**perating **S**ystem), this project will construct a robot that automatically prepares biological samples with pipettes for live cell analysis.

**ROS** will be used to control movement of the robotic arm, while cameras attached to the arm will use the **ORB** (**O**riented FAST and **R**otated **B**RIEF) algorithm to detect objects.

**ArTags** (Augmented Reality Tags) from the **ArUco** library will be used for virtual simulations.


```








